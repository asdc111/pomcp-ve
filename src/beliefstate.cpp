#include "beliefstate.h"
#include "simulator.h"
#include "influencemax.h"
// #include "utils.h"

// using namespace UTILS;

BELIEF_STATE::BELIEF_STATE()
{
    presence_belief = new MARKOV_NET();
}

void BELIEF_STATE::Free(const SIMULATOR& simulator)
{
    presence_belief->Free();
    invited.clear();
}

STATE* BELIEF_STATE::CreateSample(const SIMULATOR& simulator) const
{
    INFLUENCEMAX_STATE state;
    state.presence = presence_belief->sample_net();
    state.invited = invited;
    state.actions_left = actions_left;
    state.session_index = session_index;
    
    return simulator.Copy(state);
}

// void BELIEF_STATE::AddSample(STATE* state)
// {
//     Samples.push_back(state);
// }

void BELIEF_STATE::Copy(const BELIEF_STATE& beliefs, const SIMULATOR& simulator)
{
    presence_belief = new MARKOV_NET(*(beliefs.presence_belief));
    invited = beliefs.invited;
    actions_left = beliefs.actions_left;
    session_index = beliefs.session_index;
}

void BELIEF_STATE::Move(BELIEF_STATE& beliefs)
{
    presence_belief = beliefs.presence_belief;  // Making alias since latter will not be used
    invited = beliefs.invited;
    actions_left = beliefs.actions_left;
    session_index = beliefs.session_index;

    beliefs.invited.clear();
}

void BELIEF_STATE::print() const
{
    cout << "BELIEF STATE:" << endl;

    presence_belief->print();

    cout << "Invited Nodes: ";
    for(set<int>::iterator it = invited.begin(); it != invited.end(); ++it)
        cout << *it << " ";
    cout << endl;

    cout << "Actions left: " << actions_left << endl;
    cout << "Session index: " << session_index << endl;

    cout << endl;
}
