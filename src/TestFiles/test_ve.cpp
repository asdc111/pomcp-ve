#include <iostream>
#include <vector>
#include <assert.h>
#include <map>

#define Infinity 1e+10

using namespace std;

vector<vector<double> > queryUVals;
vector<vector<double> > inviteUVals;
unsigned* communitySizes;

int num_factors;
unsigned num_nodes;

unsigned MAX_QUERY, MAX_INVITE;
unsigned invited_size;

map<int, map<int, int> > reversePartitionMap;

void run_ve();

int main()
{
    num_factors = 3;	// Number of factors
    num_nodes = 10;

    // Initializing queryUVals
    vector<double> factor1;
    factor1.push_back(2);
    factor1.push_back(14);
    factor1.push_back(19);
    factor1.push_back(36);
    factor1.push_back(15);
    factor1.push_back(34);
    factor1.push_back(27);
    factor1.push_back(45);
    queryUVals.push_back(factor1);

    vector<double> factor2;
    factor2.push_back(2);
    factor2.push_back(7);
    factor2.push_back(17);
    factor2.push_back(38);
    factor2.push_back(15);
    factor2.push_back(24);
    factor2.push_back(35);
    factor2.push_back(57);
    queryUVals.push_back(factor2);

    vector<double> factor3;
    factor3.push_back(2);
    factor3.push_back(15);
    factor3.push_back(8);
    factor3.push_back(27);
    factor3.push_back(9);
    factor3.push_back(28);
    factor3.push_back(35);
    factor3.push_back(24);
    factor3.push_back(21);
    factor3.push_back(38);
    factor3.push_back(23);
    factor3.push_back(44);
    factor3.push_back(29);
    factor3.push_back(55);
    factor3.push_back(56);
    factor3.push_back(74);
    queryUVals.push_back(factor3);

    // Initializing inviteUVals
    for(vector<vector<double> >::iterator it = queryUVals.begin(); it != queryUVals.end(); ++it)
    {
    	vector<double> newvec;
    	for(vector<double>::iterator it2 = it->begin(); it2 != it->end(); ++it2)
    		newvec.push_back(*it2);
    	inviteUVals.push_back(newvec);
    }


    communitySizes = new unsigned[num_factors];
    communitySizes[0] = 3;
    communitySizes[1] = 3;
    communitySizes[2] = 4;

    MAX_QUERY = 3;
    MAX_INVITE = 3;
    invited_size = 1;

    map<int, int> map1;
    map1.insert(pair<int,int>(3,8));
    map1.insert(pair<int,int>(2,3));
    map1.insert(pair<int,int>(1,5));
    reversePartitionMap.insert(pair<int, map<int,int> >(0, map1));

    map<int, int> map2;
    map2.insert(pair<int,int>(3,2));
    map2.insert(pair<int,int>(2,7));
    map2.insert(pair<int,int>(1,1));
    reversePartitionMap.insert(pair<int, map<int,int> >(1, map2));

    map<int, int> map3;
    map3.insert(pair<int,int>(4,4));
    map3.insert(pair<int,int>(3,9));
    map3.insert(pair<int,int>(2,6));
    map3.insert(pair<int,int>(1,10));
    reversePartitionMap.insert(pair<int, map<int,int> >(2, map3));

    run_ve();

    return 0;
}

void run_ve()
{
	unsigned k = MAX_QUERY;

    // Finding the best query action
    vector<double> phi_factor;
    
    for(int i = 0; i <= k; ++i) // No penalty for 0 to k, and infty beyond that
        phi_factor.push_back(0);

    vector<vector<vector<int> > > query_best_asgn;  // Will contain argmax for the VE, i.e. query_best_asgn[t][i] gives asgn of vars of t^th communties when remaining variables have i 1s
    for(int t = 0; t < num_factors; ++t)
    {
        vector<vector<int> > factor_specific;
        for(int i = 0; i <= k; ++i)
        {
            vector<int> emptyvec;
            factor_specific.push_back(emptyvec);
        }
        query_best_asgn.push_back(factor_specific);
    }

    for(int t = 0; t < num_factors; ++t)    // Eliminate one community factor at a time (by merging it with phi_factor & then marginalizing)
    {
        // Going to eliminate the t^th factor now
        vector<double> new_factor;
        unsigned this_size = communitySizes[t];

        for(int i = 0; i <= k; ++i) // Construct i^th value of new_factor based on max when exactly i vars of remaining are 1
        {
            // Exactly i of the remaining variables are 1
            unsigned quota_left = k - i;    // We can have (k-i) 1s in the current & previous variables

            double best_value = -Infinity;
            vector<int> best_asgn;

            for(int this_ones = min(quota_left, this_size); this_ones >= 0; --this_ones)
            {
                // Find best combination of exactly this_ones 1s in t^th factor
                vector<int> tempvec;
                for(int a = 0; a < this_size; ++a)
                {
                    if(a > this_size-1-this_ones)
                        tempvec.push_back(1);
                    else
                        tempvec.push_back(0);
                }

                bool end = false;
                while(!end) // iterating over all combinations of this_ones 1s
                {
                    vector<int>* newvec = new vector<int>();
                    for(int a = 0; a < this_size; ++a)
                        newvec->push_back(tempvec[a]);

                    // newvec is a combination!
                    unsigned index = 0;
                    for(int a = 0; a < this_size; ++a)  ////// Assuming that 0 variable goes to LSB!!!
                        index += ((newvec->at(a)) << a);
                    
                    double value_of_combination = queryUVals[t][index] + phi_factor[this_ones+i];
                    if(value_of_combination >= best_value)
                    {
                        best_value = value_of_combination;
                        best_asgn = *newvec;
                    }

                    bool nextPresent = false;
                    for(int a = this_size-1; a >= 1; a--)
                    {
                        if((tempvec[a] == 1) && (tempvec[a-1] == 0))
                        {
                            nextPresent = true;
                            tempvec[a] = 0;
                            tempvec[a-1] = 1;
                            int numOnesAfterPoint = 0;
                            for(int j = a+1; j < this_size; ++j)
                            {
                                if(tempvec[j] == 1)
                                    numOnesAfterPoint++;
                            }
                            for(int j = this_size-1; j >= a+1; j--)
                            {
                                if(numOnesAfterPoint)
                                {
                                    tempvec[j] = 1;
                                    numOnesAfterPoint--;
                                }
                                else
                                    tempvec[j] = 0;
                            }
                            break;
                        }
                        else
                            continue;
                    }

                    if(!nextPresent)
                        end = true;
                }
            }

            new_factor.push_back(best_value);
            assert(new_factor[i] == best_value);

            query_best_asgn[t][i] = best_asgn;
        }

        phi_factor = new_factor;
    }

    double max_query_value = phi_factor[0];
    cout << "QUERY MAXIMUM VALUE: " << max_query_value << endl;


    // Finding the best invite action
    k = MAX_INVITE - invited_size;

    phi_factor.clear();

    for(int i = 0; i <= k; ++i) // No penalty for 0 to k
        phi_factor.push_back(0);

    vector<vector<vector<int> > > invite_best_asgn;
    for(int t = 0; t < num_factors; ++t)
    {
        vector<vector<int> > factor_specific;
        for(int i = 0; i <= k; ++i)
        {
            vector<int> emptyvec;
            factor_specific.push_back(emptyvec);
        }
        invite_best_asgn.push_back(factor_specific);
    }

    for(int t = 0; t < num_factors; ++t)    // Eliminate one community factor at a time
    {
        // Eliminating the t^th factor now
        vector<double> new_factor;
        unsigned this_size = communitySizes[t];

        for(int i = 0; i <= k; ++i) // Populate new_factor
        {
            // Exactly i of the remaining variables are 1
            unsigned quota_left = k - i;

            double best_value = -Infinity;
            vector<int> best_asgn;

            for(int this_ones = min(quota_left, this_size); this_ones >= 0; --this_ones)
            {
                // Find best combination of exactly this_ones 1s in t^th factor
                vector<int> tempvec;
                for(int a = 0; a < this_size; ++a)
                {
                    if(a > this_size-1-this_ones)
                        tempvec.push_back(1);
                    else
                        tempvec.push_back(0);
                }

                bool end = false;
                while(!end) // iterating over all combinations of this_ones 1s
                {
                    vector<int>* newvec = new vector<int>();
                    for(int a = 0; a < this_size; ++a)
                        newvec->push_back(tempvec[a]);
                    
                    // newvec is a combination!
                    unsigned index = 0;
                    for(int a = 0; a < this_size; ++a)  ////// Assuming that 0 variable goes to LSB!!!
                        index += ((newvec->at(a)) << a);

                    double value_of_combination = inviteUVals[t][index] + phi_factor[this_ones+i];
                    if(value_of_combination >= best_value)
                    {
                        best_value = value_of_combination;
                        best_asgn = *newvec;
                    }

                    bool nextPresent = false;
                    for(int a = this_size-1; a >= 1; a--)
                    {
                        if((tempvec[a] == 1) && (tempvec[a-1] == 0))
                        {
                            nextPresent = true;
                            tempvec[a] = 0;
                            tempvec[a-1] = 1;
                            int numOnesAfterPoint = 0;
                            for(int j = a+1; j < this_size; ++j)
                            {
                                if(tempvec[j] == 1)
                                    numOnesAfterPoint++;
                            }
                            for(int j = this_size-1; j >= a+1; j--)
                            {
                                if(numOnesAfterPoint)
                                {
                                    tempvec[j] = 1;
                                    numOnesAfterPoint--;
                                }
                                else
                                    tempvec[j] = 0;
                            }
                            break;
                        }
                        else
                            continue;
                    }

                    if(!nextPresent)
                        end = true;
                }
            }

            new_factor.push_back(best_value);
            assert(new_factor[i] == best_value);

            invite_best_asgn[t][i] = best_asgn;
        }

        phi_factor = new_factor;
    }

    double max_invite_value = phi_factor[0];

    cout << "INVITE MAXIMUM VALUE: " << max_invite_value << endl;

    if(max_query_value > max_invite_value)  // Take the best query action
    {
    	cout << endl << endl << "Query Assignment" << endl;
	 
	    bool* query_action = new bool[num_nodes];

	    vector<int> temp_asgn = query_best_asgn[num_factors-1][0];  // Assignment for the last community, since remaining are 0

	    unsigned this_comm_size = communitySizes[num_factors-1];
	    assert(this_comm_size == temp_asgn.size());

	    cout << endl;
	    cout << "Community " << num_factors << ": ";

	    for(int j = this_comm_size-1; j >= 0; j--)
	    	cout << temp_asgn[j] << flush;
	    cout << endl;

	    for(unsigned j = 0; j < this_comm_size; ++j)
	        query_action[(reversePartitionMap[num_factors-1])[j+1] - 1] = temp_asgn[j]; // partitionMap is 1-indexed everywhere, so +1 for community index, +1 for the index within community & finally -1 to get 0-indexed graph node

	    unsigned rem = 0;
	    for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)  // Counts 1s in the last community
	    	rem += *it2;

	    for(int t = num_factors - 2; t >= 0; t--)
	    {
	        temp_asgn = query_best_asgn[t][rem];

	        this_comm_size = communitySizes[t];
	        assert(this_comm_size == temp_asgn.size());

		    cout << endl;
		    cout << "Community " << t+1 << ": ";
		    for(int j = this_comm_size-1; j >= 0; j--)
		    	cout << temp_asgn[j];
		    cout << endl;

	        for(unsigned j = 0; j < this_comm_size; ++j)
	            query_action[(reversePartitionMap[t])[j+1] - 1] = temp_asgn[j];   // partitionMap is 1-indexed everywhere, so +1 for community index, +1 for the index within community & finally -1 to get 0-indexed graph node

	        for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)
	            rem += *it2;
	    }

	    // query_action now has the best query assignment!
	    cout << endl << "Best Query Action (as vector after map): ";
	    for(int i = 0; i < num_nodes; ++i)  // Assuming first (zeroth) node goes to LSB
	    	cout << query_action[i];
	    cout << endl;
	}

	else
	{
		cout << endl << endl << "Invite Assignment" << endl;

	    bool* invite_action = new bool[num_nodes];

	    vector<int> temp_asgn = invite_best_asgn[num_factors-1][0];

	    unsigned this_comm_size = communitySizes[num_factors-1];
	    assert(this_comm_size == temp_asgn.size());

	    cout << endl;
	    cout << "Community " << num_factors << ": ";

	    for(int j = this_comm_size-1; j >= 0; --j)
	    	cout << temp_asgn[j] << flush;
	    cout << endl;

	    for(unsigned j = 0; j < this_comm_size; ++j)
	        invite_action[(reversePartitionMap[num_factors-1])[j+1] - 1] = temp_asgn[j];

	    unsigned rem = 0;
	    for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)  // Counts 1s in the last community
	        rem += *it2;

	    for(int t = num_factors - 2; t >= 0; t--)
	    {
	        temp_asgn = invite_best_asgn[t][rem];

	        this_comm_size = communitySizes[t];
	        assert(this_comm_size == temp_asgn.size());

	        cout << endl;
	        cout << "Community " << t+1 << ": ";
	        for(int j = this_comm_size-1; j >= 0; j--)
	        	cout << temp_asgn[j];
	        cout << endl;

	        for(unsigned j = 0; j < this_comm_size; ++j)
	            invite_action[(reversePartitionMap[t])[j+1] - 1] = temp_asgn[j];

	        for(vector<int>::iterator it2 = temp_asgn.begin(); it2 != temp_asgn.end(); ++it2)
	            rem += *it2;
	    }

	    // invite_action now has the best invite assignment!
	    cout << endl << "Best Invite Action (as vector after map): ";
	    for(int i = 0; i < num_nodes; ++i)
	    	cout << invite_action[i];
	    cout << endl;
	}
}