#include "markovnet.h"
#include <iostream>

#define NUM_SAMPLES 61400

int main()
{
	MARKOV_NET mn;
	mn.num_vars = 3;
	mn.Factors.clear();

	FACTOR f1;
	f1.scope.clear();
	f1.scope.push_back(1);
	f1.scope.push_back(2);
	f1.potential.clear();
	f1.potential.push_back(7);
	f1.potential.push_back(1);
	f1.potential.push_back(1);
	f1.potential.push_back(4);
	mn.Factors.push_back(f1);

	FACTOR f2;
	f2.scope.clear();
	f2.scope.push_back(1);
	f2.scope.push_back(0);
	f2.potential.clear();
	f2.potential.push_back(8);
	f2.potential.push_back(2);
	f2.potential.push_back(2);
	f2.potential.push_back(5);
	mn.Factors.push_back(f2);

	FACTOR f3;
	f3.scope.clear();
	f3.scope.push_back(0);
	f3.scope.push_back(2);
	f3.scope.push_back(1);
	f3.potential.clear();
	f3.potential.push_back(10);
	f3.potential.push_back(2);
	f3.potential.push_back(3);
	f3.potential.push_back(1);
	f3.potential.push_back(2);
	f3.potential.push_back(1);
	f3.potential.push_back(1);
	f3.potential.push_back(8);
	mn.Factors.push_back(f3);

	mn.observed.clear();
	mn.add_observation(1,0);
	mn.add_observation(1,0);
	// mn.add_observation(2,0);

	int counts[8];
	for(int i = 0; i < 8; ++i)
		counts[i] = 0;

	bool* asgn;
	for(int i = 0; i < NUM_SAMPLES; ++i)
	{
		bool* asgn = mn.sample_net();
		counts[asgn[0]*4 + asgn[1]*2 + asgn[2]] += 1;
	}

	std::cout << "000: " << counts[0] << std::endl;
	std::cout << "001: " << counts[1] << std::endl;
	std::cout << "010: " << counts[2] << std::endl;
	std::cout << "011: " << counts[3] << std::endl;
	std::cout << "100: " << counts[4] << std::endl;
	std::cout << "101: " << counts[5] << std::endl;
	std::cout << "110: " << counts[6] << std::endl;
	std::cout << "111: " << counts[7] << std::endl;
}
