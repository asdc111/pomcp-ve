import numpy as np
from sys import argv

input_graph = np.loadtxt(argv[1], delimiter = ' ', dtype = int)

num_edges = len(input_graph)

max_index = -1;
for e in input_graph:
	max_index = max(max_index, e[0], e[1])

num_nodes = max_index

outfile = open(argv[2], 'w')
outfile.write("%d %d\n" % (num_nodes, num_edges))	# Adding the number of nodes & edges

# Adding each of the factors to file
for e in input_graph:
	outfile.write("2\n")	# Number of arguments of factor
	# MN file is 0-indexed for vertices!
	outfile.write("%d %d\n" % (e[0]-1, e[1]-1))	# Arguments of factor (edge vertices)
	outfile.write("3 2 2 3\n")	# Factor values

outfile.close()
