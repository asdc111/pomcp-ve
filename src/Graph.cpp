#include "Graph.h"
#include<fstream>
#include<time.h>
#include<stdlib.h>

using namespace std;

void Graph::makeUndirected()
{
	for (map<int, map<int, int> * >::iterator it= adjList.begin();it!=adjList.end();it++)
	{
		int node = it->first;
		map<int, int> *nodeList = it->second;
		
		for (map<int, int>::iterator iter = nodeList->begin(); iter!= nodeList->end(); iter++)
		{
			int endNode = iter->first;
			map<int, int> *endNodeList = adjList.find(endNode)->second;
			endNodeList->insert(pair<int, int>(node, iter->second));///add the reverse edge
		}
	}
}

int Graph::numUndirectedEdges()
{
        int result=0;
        map<pair<int, int>, int > seen;
        for (map<int, map<int, int> * >::iterator it= adjList.begin();it!=adjList.end();it++)
    {
                int node=it->first;
                map<int, int> *nodeList = it->second;

                for (map<int, int>::iterator iter = nodeList->begin(); iter!= nodeList->end(); iter++)
                {
                        int endNode=iter->first;

                        if (seen.find(pair<int, int>(node, endNode))==seen.end()&&seen.find(pair<int, int>(endNode, node))==seen.end())
                        {
                                result++;
                                seen.insert(pair< pair<int, int>, int>(pair<int, int>(node, endNode),1));
                                seen.insert(pair< pair<int, int>, int>(pair<int, int>(endNode, node),1));
                        }
                }
        }
        return result;
}



Graph::~Graph()
{
	///delete adjList
	for (map<int, map<int, int> * >::iterator it=adjList.begin();it!=adjList.end();it++)
	{
		map<int, int> *curr = it->second;
		delete curr;
	}
	adjList.clear();

	uncertainEdgeList.clear();

	///delete adjMatrix
	for (int i=0;i<numNodes;i++)
		delete [] adjMatrix[i];
	delete adjMatrix;
}


Graph::Graph(int nodes, int edges)
{
	numNodes = nodes;
	numEdges = edges;
}

Graph::Graph()
{   
}
    
Graph::Graph(string graphFile, bool ucertain, double existencePr, double propagationPr)
{
	T=1;
	srand(time(NULL));
	graphfile = graphFile;
	existenceProb = existencePr;
	propagationProb = propagationPr;
	isUncertain = ucertain;
        
	int uncertainEdgeListCounter = 0;
	numUncertainEdges = 0;
	ifstream file(graphFile.c_str());
		
	int startVertex, endVertex, edgeCertain, edgeUncertain;
	if (file.is_open())
	{
		while(file>>startVertex>>endVertex>>edgeCertain>>edgeUncertain)
		{
			if (adjList.find(startVertex)==adjList.end())
			{
				map <int, int> *vertexList = new map<int, int>();
				adjList.insert(pair<int, map<int, int > * >(startVertex, vertexList));
			} 
			if (adjList.find(endVertex)==adjList.end())
			{
				map <int, int> *vertexList = new map<int, int>();
				adjList.insert(pair<int, map<int, int> * >(endVertex, vertexList));
			}
                
			map<int, int> *list = adjList.find(startVertex)->second;
			if (edgeCertain==0)///uncertain edge
			{
				double r3 = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
				if (isUncertain)/////you are right now building an uncertain graph for the real Simulator object
				{
					list->insert(pair<int, int>(endVertex, uncertainEdgeListCounter));
					uncertainEdgeList.insert(pair<int, pair<int, int> >(uncertainEdgeListCounter++, pair<int, int>(startVertex, endVertex)));
				}
		    		else if (r3<existenceProb)///otherwise you are making an MDP....randomly keep or remove uncertain edges
		    		{
		    			list->insert(pair<int, int>(endVertex, -1));
					edgeCertain=1;
					edgeUncertain=0;
		    		}
				else////edge removed randomly
				{
					edgeUncertain=0;
				}
			}
			else ///certain edge...will have a -1
			{
				list->insert(pair<int, int>(endVertex, -1));
			}
			numUncertainEdges+=edgeUncertain;
			numEdges+=(edgeCertain+edgeUncertain);
		}
		file.close();
		numNodes = (int)adjList.size();
            
		////initialize adjMatrix
		adjMatrix = new double*[numNodes];
            
		for (int i=0;i<numNodes;i++)
			adjMatrix[i] = new double[numNodes];

		for (int i=1;i<=numNodes;i++)
		{
			for (int j=1;j<=numNodes;j++)
			{
				if (i==j)
				{
					adjMatrix[i-1][j-1]=0;
					continue;
				}
					
				map<int, map<int, int> * >::iterator it = adjList.find(i);

				if (it != adjList.end())
				{
					map<int, int> * list = it->second;	
					map<int, int>::iterator iter = list->find(j);
					
					if (iter != list->end())
					{
						if (iter->second==-1)///certain edge
						{
							adjMatrix[i-1][j-1]=1;
						}
						else
						{
							adjMatrix[i-1][j-1]=existenceProb;
						}
					}
					else
					{
						adjMatrix[i-1][j-1]=0;
					}
				}
				else
				{
					cout<<"Big error: "<<i<<" "<<j<<"\n";
				}
			}
		}
		////adjMatrix initialized
	}
}
