#ifndef MARKOVNET_H
#define MARKOVNET_H

#include <vector>

class FACTOR
{
public:
	FACTOR();
	std::vector<unsigned> scope;	// Variables in scope of this factor
	std::vector<float> potential;	// (Flattened) Table representing the factor
	float get_factor_value(bool* asgn) const;	// Function that gives the potential value for a given assignment
	void print() const;
	void Free();
};

class MARKOV_NET
{
public:
	MARKOV_NET();
	MARKOV_NET(const MARKOV_NET& mn);
	unsigned num_vars;
	// std::vector<std::vector<unsigned> > adj_list;	// Graph corresponding to the Markov Network
	std::vector<FACTOR> Factors;
	std::vector<std::pair<unsigned, bool> > observed;

	bool* sample_net(unsigned n_steps=1000 /*number of burnout samples*/) const;	// Gibbs sampling is used to sample from the markov network
	void add_observation(unsigned var, bool val);
	void print() const;
	void Free();
};

#endif // MARKOVNET_H
